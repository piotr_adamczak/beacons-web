# encoding: utf-8 
class CreateResources < ActiveRecord::Migration
  def change
    create_table :resources do |t| 
      t.text :url 
      t.integer :type 
      t.timestamps 
    end
  end
end

