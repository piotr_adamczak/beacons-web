# encoding: utf-8 
class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t| 
      t.text :name 
      t.text :url 
      t.integer :floor 
      t.float :width 
      t.float :height 
      t.timestamps 
    end
  end
end

