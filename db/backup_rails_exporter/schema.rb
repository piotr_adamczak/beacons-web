# encoding: utf-8 
# Instead 
# of editing this file, please use the migrations feature of Active Record to 
# incrementally modify your database, and then regenerate this schema definition. 
# 
# This schema file was created with MySQL Workbench Rails Exporter Plugin. 

ActiveRecord::Schema.define(:version => 20140506225505) do 


  create_table "beacons", :force => true do |t| 
      t.string "uuid", :null => false  
      t.integer "major" 
      t.integer "minor" 
      t.integer "color" 
      t.datetime "created_at", :null => false
      t.datetime "updated_at", :null => false 
  end

  create_table "places", :force => true do |t| 
      t.text "name" 
      t.datetime "created_at", :null => false
      t.datetime "updated_at", :null => false 
  end

  create_table "resources", :force => true do |t| 
      t.text "url" 
      t.integer "type" 
      t.datetime "created_at", :null => false
      t.datetime "updated_at", :null => false 
  end

  create_table "rooms", :force => true do |t| 
      t.text "name" 
      t.text "url" 
      t.integer "floor" 
      t.float "width" 
      t.float "height" 
      t.datetime "created_at", :null => false
      t.datetime "updated_at", :null => false 
  end

  create_table "actions", :force => true do |t| 
      t.float "x" 
      t.float "y" 
      t.float "proximity" 
      t.integer "type" 
      t.text "content" 
      t.datetime "created_at", :null => false
      t.datetime "updated_at", :null => false 
  end

end