# encoding: utf-8 
class CreateActions < ActiveRecord::Migration
  def change
    create_table :map_actions do |t| 
      t.float :x 
      t.float :y 
      t.float :proximity 
      t.integer :type 
      t.text :content 
      t.timestamps 
    end
  end
end

