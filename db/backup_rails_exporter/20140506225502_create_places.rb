# encoding: utf-8 
class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t| 
      t.text :name 
      t.timestamps 
    end
  end
end

