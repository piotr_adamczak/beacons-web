# encoding: utf-8 
# This file should contain all the record creation needed to seed the database with its default values. 
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup). 
# 
# Examples: 
# 
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }]) 
#   Mayor.create(name: 'Emanuel', city: cities.first) 
# 
# This seed file was created with MySQL Workbench Rails Exporter Plugin. 


puts ''
puts '*** Seeeds in progress ...'
puts '--> 30 seed units for table: beacons'

beacons_list = [ 
  [ 1, 'Depeche Mode_7', 45866, 53277, 21896 ], 
  [ 2, 'Rails_6', 93470, 38351, 51942 ], 
  [ 3, 'Al Bundy_0560304', 68678, 58898, 93044 ], 
  [ 4, 'Neverland_0647972', 4747, 73609, 32824 ], 
  [ 5, 'Tocotronic_932977', 65152, 7269, 63164 ], 
  [ 6, 'Halo_47422314', 89766, 90921, 6057 ], 
  [ 7, 'Neverland_53942090', 50071, 38415, 27709 ], 
  [ 8, 'Neverland_49077810', 68846, 86825, 62955 ], 
  [ 9, 'Spock_982335', 59112, 84599, 41209 ], 
  [ 10, 'Halo_4542115', 80241, 3306, 53445 ], 
  [ 11, 'Yello_7586', 84204, 15977, 21276 ], 
  [ 12, 'Depeche Mode_020407', 93790, 23992, 18090 ], 
  [ 13, 'X-Files_616', 38582, 38773, 49975 ], 
  [ 14, 'Pussy Riot_85', 95541, 55615, 14816 ], 
  [ 15, 'Kayleigh_15244911', 31925, 62927, 12672 ], 
  [ 16, 'Quantum Leap_824320', 90168, 42650, 14203 ], 
  [ 17, 'Kayleigh_18010321', 78316, 45531, 34953 ], 
  [ 18, 'Venus_9626', 90893, 25013, 86086 ], 
  [ 19, 'Neverland_6874', 95137, 63274, 43934 ], 
  [ 20, 'Rails_7998255', 10344, 41403, 57672 ], 
  [ 21, 'Lua_78787708', 52499, 46333, 6520 ], 
  [ 22, 'Marillion_661988', 54395, 13920, 45035 ], 
  [ 23, 'Fringe_43584483', 21152, 99912, 15361 ], 
  [ 24, 'Quantum Leap_007734', 68250, 68057, 20527 ], 
  [ 25, 'Spock_8007622', 8107, 38883, 95216 ], 
  [ 26, 'Kayleigh_26277421', 1132, 19183, 98324 ], 
  [ 27, 'Venus_13', 60102, 17689, 82836 ], 
  [ 28, 'Zombie_22', 10164, 21942, 63472 ], 
  [ 29, 'Ultravox_676605', 22701, 31878, 69984 ], 
  [ 30, 'Tocotronic_5', 55392, 58799, 32967 ] 
] 

memU_PRIMARY = [] 
@mem_beacons = Array.new
CntSeeds = 0 
Beacon.delete_all 
beacons_list.each do |val| 
  if memU_PRIMARY.include? (val[0].to_s) 
    puts '  ! Seed reduced by unique attribute, index name: PRIMARY' 
    break 
  end 
  @mem_beacons << Beacon.create!(id: val[0], uuid: val[1], major: val[2], minor: val[3], color: val[4]) 
  CntSeeds +=1 
  memU_PRIMARY << (val[0].to_s) 
end 

puts '  * ' + CntSeeds.to_s + ' seed units created for table: beacons' 
puts '--> 30 seed units for table: places'

places_list = [ 
  [ 1, 'Depeche Mode_Eurythmics_Marillion_Woodstock_Venus_Opium_Tocotronic_iPhone_Depeche Mode_Neverland_iPhone_Pussy Riot_Neverland_Lua_Genesis_Jethro Tull_Kayleigh_Lua_Marillion_Kayleigh' ], 
  [ 2, 'Chaplin_Jethro Tull_Genesis_Rails_Rails_Neverland_Spock_Spock_Yello_Lua_' ], 
  [ 3, 'iPhone_Lua_Neverland_Rails_Kayleigh_Pussy Riot_Zombie_Depeche Mode_Rails_iPhone_Opium_Neverland_Venus_Al Bundy_Halo_Quantum Leap_Opium_X-Files_Genesis_Zombie_Genesis_Venus_Spock_Lua_Venus_Al Bundy_Eurythmics_Spock_Spock_Lua_Pussy Riot_Tocotr' ], 
  [ 4, 'Spock_Halo_Tocotronic_Eurythmics_Al Bundy_Marillion_Chaplin_Eurythmics_X-Files_Ultravox_Tocotronic_Lua_Al Bundy_Pussy Riot_Neverland_Big Bang Theory_Opium_Kayleigh_Fringe_Kayleigh_Quantum Leap_Rails_Spock_Opium_Halo_Venus_Jethro Tull_Rails_iPhone_Al Bund' ], 
  [ 5, 'Neverland_Ultravox_Fringe_B' ], 
  [ 6, 'Eurythmics_Spock_Ha' ], 
  [ 7, 'Marillion_Lua' ], 
  [ 8, 'Lua_Depeche Mode_Halo_Venus_Spock_Pussy Riot_Rails_iPhone_Big Bang Theory_Zombie_Opium_Al Bundy_Woodstock_Al Bundy_Jethro Tull_Marilli' ], 
  [ 9, 'Tocotronic_Rails_Neverland_Al Bundy_Depeche Mode_Zombie_Eurythmics_Jethro Tull_Pussy Riot_Pussy Riot_Depeche Mode_Opium_Yello_Jethro Tull_Venus_Zomb' ], 
  [ 10, 'Genesis_Lua_Venus_X-Files_Genesis_Tocotronic_Spock_Lua_Genesis_Rail' ], 
  [ 11, 'Fringe_Chaplin_Opium_Al Bundy_' ], 
  [ 12, 'Pussy Riot_Lua_Chaplin_Rails_Jethro Tull_Venus' ], 
  [ 13, 'Zombie_Al Bundy_Chaplin_Marillio' ], 
  [ 14, 'Yello_Genesis_Jethro Tull_Tocotronic_Marillion_Tocotronic_Rails_Rails_Tocotronic_Al Bundy_Fringe_Fringe_Opium_Quantum Leap_Quantum Leap_Jethro Tull_Fringe_Genesis_Quantum Leap_Eurythmics_Tocotronic' ], 
  [ 15, 'Big Bang Theory_Rails_Depeche Mode_Yello_' ], 
  [ 16, 'Quantum Leap_X-Files_Al Bundy_Pussy Riot_Spock_Marillion_Big Bang Theory_Lua_Neverland_Ultravox_Big Bang Theory_Lua_Zombie_M' ], 
  [ 17, 'Lua_Tocotronic_Fringe_Eurythmics_Jethro Tull_Yello_Zombie_Genesis_Big Bang Theory_Dep' ], 
  [ 18, 'Eurythmics_Neverland_Chaplin_Venus_Jethro Tull_Lua_iPhone_Spock_Opium_Woodstock_Tocotronic_Venus_Spock_Kayleigh_Yello_Pussy Riot_Jethro' ], 
  [ 19, 'Lua_Kayleigh_Genesis_Chaplin_Kayleigh_Marillion_Halo_Chaplin_Fringe_Ultravox_Neverland_Rails_Woodstock_Genesis_Spock_Genesis_Jethro Tull_Ch' ], 
  [ 20, 'Genesis_Ultravox_Marillion_Al Bundy_Genesis_Rails_Lua_Eurythmics_Zombie_Depeche Mode_Ultravox_Yello_Eurythmics_Pussy R' ], 
  [ 21, 'Jethro Tull_Halo_' ], 
  [ 22, 'Venus_Venus_Spock_iPhone_Ultravox_Quantum Leap_Big Bang Theory_Big Bang Theory_Depeche Mode_Tocotronic_Big Bang Theory_Neverland_Ye' ], 
  [ 23, 'Fringe_Chaplin_Spock_Spock_Marillion_Genesis_Spock_Jethro' ], 
  [ 24, 'Genesis_Lua_Marillion_Woodstock_Zombie_Lua_Al Bundy_' ], 
  [ 25, 'Al Bundy_Tocotronic_Pussy Riot_Venus_iPhone_Genesis_Neverland_Lua_Kayleigh_Zombie_Zombie_Woodstock_Woodstock_iPhone_Depeche Mode_Zombie' ], 
  [ 26, 'Marillion_Yello_Rails_Venus_Opium_Opium_Eurythmics_Yello_Venus_Eurythmics_Big Bang Theory_iPhone_Chaplin_Chaplin_Neverland_Al Bundy_Opium_Woodstock_Neverland_Rails_' ], 
  [ 27, 'Spock_Al Bundy_Eurythmics_Rails_iPhone_Big Bang Theory_Depeche Mode_X-Files_Fringe_Jethro Tull_Pussy Riot_Eurythmics_Al Bundy_Rails_Tocotronic_Spock_Chaplin_Pussy Riot_Yello_Yello_Lua_Never' ], 
  [ 28, 'X-Files_Pussy Riot_Spock_Neverland_Marillion_Eurythmics_Zombie_Opium_Halo_Neverland_Spock_Rails_Chaplin_Opium_Pussy Riot_Rails_Marillion_Opium_Pussy Riot_Lua_Depeche Mode_Neverland_Tocotronic_Al Bundy_Big Bang The' ], 
  [ 29, 'iPhone_Al Bundy_Genesis_Chaplin_Jethro Tull_Opium_Pussy Riot_Pussy Riot_Yello_Ultravox_Neverland_Marillion_Quantum Leap_Spock_Kayleigh' ], 
  [ 30, 'Marillion_Spock_Neverland_Halo_Pussy Riot_Halo_Spock_Neverland_Yello_Genesis_Kayleigh_Pussy Riot_Ultravox_X-Files_Rails_Venus' ] 
] 

memU_PRIMARY = [] 
@mem_places = Array.new
CntSeeds = 0 
Place.delete_all 
places_list.each do |val| 
  if memU_PRIMARY.include? (val[0].to_s) 
    puts '  ! Seed reduced by unique attribute, index name: PRIMARY' 
    break 
  end 
  @mem_places << Place.create!(id: val[0], name: val[1]) 
  CntSeeds +=1 
  memU_PRIMARY << (val[0].to_s) 
end 

puts '  * ' + CntSeeds.to_s + ' seed units created for table: places' 
puts '--> 30 seed units for table: resources'

resources_list = [ 
  [ 1, 'Fringe_Depeche Mode_X-Files_Genesis_Quantum Leap_Al Bundy_Yello_Halo_Woodstock_Halo_Yello_Neverland_Depeche Mode_Kayleigh_Spock_Lua_Chap', 76618 ], 
  [ 2, 'Lua_iPhone_Halo_Tocotronic', 33851 ], 
  [ 3, 'Big Bang Theory_Opium_Marillion_Fringe_Chaplin_Zombie_Woodstock_Marillion_Jethro Tull', 80492 ], 
  [ 4, 'Eurythmics_Big Bang Theory_Depeche Mode_Ma', 38061 ], 
  [ 5, 'Lua_Kayleigh_Quantum Leap_iPhone_Big Bang Theory_Venus_X-Files_X-Files_X-Files_Ultravox_Eurythmics_Opium_Al Bundy_Ultravox_Neverland_Woodstock_Halo_Woodstock_Pussy Riot_Kayleigh_Venus_Neverland_Yello_Kayleigh_Pussy Riot_Tocotro', 88079 ], 
  [ 6, 'Eurythmics_Tocotronic_Spock_Woodstock_Big Bang Theory_Spock_Woodstock_Jethro Tull_Chapl', 16103 ], 
  [ 7, 'Chaplin_Rails_Opium_Venus_Woodstock_Ultravox_Quantum Leap_X-Files_Woodstock_Marillion_Chaplin_Jethro Tull_J', 68758 ], 
  [ 8, 'Big Bang Theory_Depeche Mode_Mar', 70859 ], 
  [ 9, 'Kayleigh_Zombie_Quantum Leap_Rails_X-Files_Jethro Tull_Woodst', 79428 ], 
  [ 10, 'Big Bang Theory_Marillion_Marillion_Lua_Marillion_Tocotronic_Yello_Halo_Depeche Mode_Chaplin_Chaplin_Zombie_Ka', 23842 ], 
  [ 11, 'Venus_Fringe_Depeche Mode_Fringe_iPhone_Opium_Lua_Pussy Riot_Lua_Jethro Tull_Jethro Tull_Pussy Riot_Lua_Depeche Mode_Rails_Zombie_Spock_Kayleigh_Woodstock_Venus_Ultravox_Jethro Tull_Al Bundy_Marillion_Quantum Leap_Quantum Leap_Quantum Leap_Ultravo', 37737 ], 
  [ 12, 'Kayleigh_iPhone_Spock_Depeche Mode_Halo_Rails_Woodstock_iPhone_Depeche Mode_Pussy Riot_Al Bu', 92083 ], 
  [ 13, 'Neverland_iPhone_Genesis_Halo_Venus_Depeche Mode_Quantum Leap_Big Bang Theory_Lua_Jethro Tull_Eury', 75586 ], 
  [ 14, 'Al Bundy_Ultravox_Opium_Halo_Kayleigh_iPhone_Neverland_Spock_Venus_Genesis_Marillion_Jethro Tull_Big Bang Theory_Big Bang Theory_Fringe_Opium_Venus_Kayleigh_Venus_Halo_Eur', 35753 ], 
  [ 15, 'Pussy Riot_Jethro Tull_Marillion_Quantum Leap_Yello_Kayleigh_Big Bang Theory_Lua_Pussy Riot_Tocotronic_Woodstock_Depeche Mode_Chaplin_Rails_Venus_Pussy Riot_Rails_Zombie_Al Bundy_X-Files_Fringe_Zombie_Neverland_Halo_Spock_Pussy Riot_Venus_V', 65989 ], 
  [ 16, 'Quantum Leap_Jethro Tull_Neverland_Rails_iPhone_Woodstock_Lua_X-Files_Halo_Yello_Depeche Mode_Kayleigh_Venus_Zombie_iPhone_Neverland_Neverland_Venus_Yello_Rails_Genesis_Yello_Pussy Riot_D', 24698 ], 
  [ 17, 'Fringe_Eurythmics_Venus_Pussy Riot_Quantum Leap_Venus_Rails_Kayleigh_Woodstock_Rails_Depeche Mode_Al Bundy_Rails_Quantum Leap_Lua_Opium_Al Bundy_Jethro Tull_Woodstock_Jethro Tull_Rails_Opium_Big Bang Theory_Tocotronic_Halo_Lua_E', 9181 ], 
  [ 18, 'Zombie_Pussy Riot_Al Bundy_Rails_Marillion_Opium_X-Files_Lua_Rails_Fringe_Marillion_Halo_Yello_Genesis_Yello_Marillion_Depeche Mode_Yello_Woodstock_Fringe_Al Bundy_Ultravox_Big Bang Theory_Yello_Quantum Leap_Opium_Depeche Mode_Dep', 82463 ], 
  [ 19, 'Rails_Neverland_Neverland_Depeche Mode_X-Files_Rails_Halo_Kayleigh_Kayleigh_Venus_Genesis_Depeche Mode_Spock_Quantum Leap_Fringe_', 84870 ], 
  [ 20, 'Chapli', 18798 ], 
  [ 21, 'Ultravox_Spock_Rails_Pussy Riot_Jethro Tull_Eurythmics_Halo_Eurythmics_Fringe_Rails_Kayleigh_', 88652 ], 
  [ 22, 'iPhone_Lua_Spock_Venus_Ultravox_Woodstock_Halo_Genesis_Eurythmics_Neverland_Big Bang Theory_Zombie_Tocotronic_Depeche Mode_Eurythmics_Lua_Ultravox_Fr', 43393 ], 
  [ 23, 'Yello_Cha', 26629 ], 
  [ 24, 'Ultravox_Genesis_Fringe_Halo_Jethro Tull_Fringe_Yello_Tocotronic_Quantum Leap_Opium_Jethro Tull_X-Files_Fringe_Chaplin_Ultra', 69817 ], 
  [ 25, 'Tocot', 11453 ], 
  [ 26, 'Rails_iPhone_Jethro Tull_Fringe_iPhone_Al Bundy_X-Files_Ultravox_Kayleigh_Spock_Venus_Woodstock_Depeche Mode_iPhone_Yello_Jethro Tull_Jethro Tull_Chaplin_Opium_Ultravox_Venus_Marillion_Venus_Quantum Leap_Lua_Jethro Tull_Jethro Tul', 28155 ], 
  [ 27, 'Quantum Leap_Tocotronic_Eurythmics_Ultravox_Genesis_Pussy Riot_Woodstock_iPhone_Pussy Riot_Depeche Mode_Jethro Tull_Depeche Mode_Genesis_Depeche Mode_Fringe_Tocotronic_Big Bang Theory_Lua_Zombie_Genesis_Fringe_Rails_Chaplin_Woodstock_De', 12018 ], 
  [ 28, 'Yello_Big Bang Theory_Big Bang Theory_Venus_Depeche Mode_Ultravox_Fringe_Kayleigh_Pussy Riot_Rails_Big Bang Theory_Zombie_Venus_Kayleigh_Al Bundy_Chaplin_Zombie_Woodstock_Rails_Venus_Genesis_Spock_Pussy Riot_', 22254 ], 
  [ 29, 'Al Bundy_Lua_Kayleigh_Yello_Al Bundy_Neverland_Jethro Tull_', 85952 ], 
  [ 30, 'Pussy Riot_iPhone_Quantum Leap_Rails_Quantum Leap_Genesis_Al Bundy_Zombie_Fringe_Quantum Leap_Opium_Jethro Tull_Depeche Mode_Lua_Depeche Mode_Pussy Riot_Opium_Marillion_X-Files_Chaplin_Jethro Tull_Chaplin', 29922 ] 
] 

memU_PRIMARY = [] 
@mem_resources = Array.new
CntSeeds = 0 
Resource.delete_all 
resources_list.each do |val| 
  if memU_PRIMARY.include? (val[0].to_s) 
    puts '  ! Seed reduced by unique attribute, index name: PRIMARY' 
    break 
  end 
  @mem_resources << Resource.create!(id: val[0], url: val[1], type: val[2]) 
  CntSeeds +=1 
  memU_PRIMARY << (val[0].to_s) 
end 

puts '  * ' + CntSeeds.to_s + ' seed units created for table: resources' 
puts '--> 30 seed units for table: rooms'

rooms_list = [ 
  [ 1, 'X-Files_Venus_Zombie_Venus_Ultravox_Venus_Lua_Quantum Leap_Spock_Venus_Marillion_Fringe_Kayleigh_Yello_Marillion_Jethro Tull_Neverland_Fringe_X-Files_Zombie_Depeche Mode_Lua_Rails_Halo_Kayleigh_Tocotronic_Al Bundy_Ha', 'Venus_Halo_Spock_Kayleigh_Ultravox_Spock_Pussy Rio', 63294, 79860.1, 62780.347 ], 
  [ 2, 'Marillion_Neverland_iPhone_Woodstock_Depeche Mode_Kayleigh_Kayleigh_Woodstock_Big Bang Theory_Lua_Tocotronic_Halo_Zombie_', 'Zombie_Tocotronic_Jethro Tull_Ultravox_Fringe_Pussy Riot_Opium_Genesis_Fringe_Opium_Neverland_Tocotronic_Pussy Riot_Zombie_Ultravox_Opium_Depeche Mode_Chaplin_Neverland_Genesis_Opium_iPhone_Rails_Big Bang Theory_Venus_Woodstock_iPhone_Depeche Mode_Tocotron', 66190, 40279.585, 55546.494 ], 
  [ 3, 'Yello_Pussy Riot_Genesis_Big Bang Theory_Quantum Leap_iPhone_Al Bundy_Eurythmics_Pussy Riot_Lua_Opium_Spock_Jethro Tull_Jethro Tull_Jethro Tull_Chaplin_Pussy ', 'Chaplin_iPhone_Rails_Opium_Woodstock_Neverland_Zombie_Big Bang Theory_Marillion_Chaplin_Zombie_Opium_Spock_D', 47499, 5935.445, 91798.440 ], 
  [ 4, 'Zombie_Opium_Depeche Mode_Woodstock_Eurythmics_Rails_Rails_Zombie_Lua_Eurythmics_Depeche Mode_Halo_Zombie_Pussy Riot_Woodstock_Big Bang Theory_Yello_Yello_Marillion_Halo_Lua_Lua_Woodstock_Fringe_Kayleigh_Tocotronic_Venus', 'Spock_Rails_iPhone_Eurythmics_Rails_X-Files_Eurythmics_Al Bundy_Eurythmics_Eurythmics_Fringe_Rails_Rails_Neverland_iPhone_Marillion_iPhone_Chaplin_Woodstock_Lua_Depeche Mode', 65205, 94199.882, 66093.204 ], 
  [ 5, 'Al Bundy_Eurythmics_Chaplin_Jethro Tull_Opiu', 'Genesis_Al Bundy_Al Bundy_X-Files_X-Files_X-Files_Opium_Eurythmics_Marillion_Venus_Yello_Pussy Riot_Opium_Marillion_Tocotronic_iPhone_Eurythmics_Neverland_Jethro Tull_Venus_Zombie_Al Bundy_Halo_Lua_X-Files_Venus_Genesis_Tocotronic_Venus_Kayleigh_S', 40588, 52631.526, 9154.404 ], 
  [ 6, 'Genesis_iPhone_Eurythmics_Tocotronic_Quantum Leap_Quantum Leap_iPhone_Eurythmics_Eurythmics_Lua_Neverland_Eurythmics_Yello_Kayleigh_Al Bundy_Eurythmics_Opium_Zombie_Depeche Mode_iPhone_iPhone_Jethro Tu', 'iPhone_Jethro Tull_Quantum Leap_Pussy Riot', 3769, 33511.79, 54403.491 ], 
  [ 7, 'Genesis_Lua_Rails_iPhone_Al Bundy_Zombie_Chaplin_iPhone_Yello_Jethro Tull_', 'Zombie_Depeche Mode_Big Bang Theory_Yello_U', 22040, 10147.255, 43752.243 ], 
  [ 8, 'Opium_Woodstock_Woodstock_Rails_Opium_Tocotronic_X-Files_Woodstock_Tocotr', 'Zombie_Genesis_Jethro Tull_Halo_Tocotronic_Zombie_Tocotronic', 42861, 64167.464, 10247.182 ], 
  [ 9, 'Neverland_Yello_X-Files_Halo_Quantum Leap_Zombie_Opium_Venus_Big Bang Theory_Eurythmics_Zombie_Tocotronic_Woodstock_Al Bundy_Neverland_Jethro Tull_Zombie_Fringe_Fringe_Pussy Riot_X-Files_Opium_Chaplin_Ultravox_Ultravox_Spo', 'Woodstock_Big ', 99467, 26992.500, 68732.748 ], 
  [ 10, 'Rails_X-Files_Eurythmics_Venus_Halo_Marillion_X-Files_Spock_Kayleigh_Eurythmics_X-Files_Fringe_Opium_Jethro Tull_Genesis_Ultravox_Genesis_Rails_iPhone_Neverland_Lua_Pussy Riot_Rails_Al Bundy_Venus_Jethro Tull_Opium_Zombie_Tocotronic_Ultravox_', 'Lua_Kayleigh_Genesis_Spock_Fringe_Rails_Kayleigh_Halo_iPhone_Halo_Rails_Yello_Ultravox_X-Files_Al Bundy_Zombie_Big Bang Theory_Kayleigh_Eurythmics_Ultravox_Al Bundy_Pussy Riot_X-Files_Chaplin_Opi', 7304, 55652.383, 48143.391 ], 
  [ 11, 'Opium_Yello_Spock_Pussy Riot_Al Bundy_Halo_Kayleigh_Tocotronic_Opium_Opium_Genesis_Depeche Mode_Eurythmic', 'Venus_Genesis_Eurythmics_Kayleigh_Quantum Leap_Neverland_Big Bang Theory_Marillion_Jethro T', 9886, 49326.133, 21625.438 ], 
  [ 12, 'Zombie_X-Files_Jethro Tull_Lua_Ultravox_Zombie_Halo_Opium_Opium_Depeche Mode_Neverland_Mari', 'Chaplin_Fringe_Eurythmics_Kayleigh_Jethro Tull_Genesis_Eurythmics_Woodstock_Genesis_X-Files_Ultravox_Zombie_Genesis_Halo_Opium_Spock_Neverland_Marillion_Jethro Tull_Woodstock_Kayleigh_Al ', 91569, 95952.569, 99789.401 ], 
  [ 13, 'Ultravox_Kayleigh_Big Bang Theory_Opium_Depeche Mode_Opium_Zombie_Rails_Fringe_Opium_Venus_Halo_Halo_Zombie_Zombie_Pussy Riot_iPhone_Neverland_Chaplin_', 'Rails_Pussy Riot_Fringe_Fringe_Eurythmics_Halo_Fringe_Ultravox_Woodstock_Ultravox_Chaplin_Eurythmics_Genesis_Tocotronic_Neverland_Kayleigh_Woodstock_iPhone_Pussy Riot_Ultravox_Zombie_Yello_Venus_Ultravox_Rai', 84195, 62800.768, 39666.585 ], 
  [ 14, 'Ultravox_Marillion_Rails_Venus_Opium_Halo_Pussy Riot_Marillion_X-Files_Lua_Genesis_Marillion_Al Bundy_Rails_', 'Opium_Rails_Rails_Tocotr', 65247, 98199.280, 78277.6 ], 
  [ 15, 'Big Bang Theory_Yello_F', 'Genesis_Halo_Tocotronic_Halo_Marillion_X-Files_X-Files_X-Files_Genesis_Tocotronic_Rails_Marillion_Marillion_Fringe_Neverland_X-Files_Venus_Yello_Zombie_X-Files_Depeche Mode_Genesis_Kayleigh_Ultravox_Ul', 77572, 45448.419, 63633.647 ], 
  [ 16, 'Venus_Halo_Ultravox_Eurythmics_Depeche Mode_Ultravox_Zombie_Chaplin_Lua_Venus_Quantum Leap_Yello_Lua_Yello_Ultravox_Jethro Tull_Genesis_Venus_Big Bang Theory_Fringe_Chaplin_Woodstock_Big Ban', 'Venus_Woodstock_Neverland_Tocotronic_Zombie_Kayleigh_Fringe_Kayleigh_Chaplin_Jethro Tull_Chaplin_Fringe_Kayleigh_Venus_X-Files_Zombie_Venus_Kayleigh_Venus_Spock_Lua_Kayleigh_Pussy Riot_Pussy Riot_Rails_Zombie_Tocotronic_Al Bundy_Quantum Leap_Dep', 89610, 67784.329, 23684.452 ], 
  [ 17, 'Genesis_Depeche Mode_Spock_Pussy Riot_Rails_Big Bang Theory_Woodstock_Fringe_Fringe_Tocotronic_Ultravox_Yello_Spock_Tocotronic_Fringe_Fringe_Al Bundy_Venus_Tocotronic_Fringe_K', 'Rails_Venus_Big Bang Theory_Quantum Leap_Quantum Leap_Lua_Chaplin_Spoc', 1752, 41284.538, 10226.640 ], 
  [ 18, 'Al Bundy_Genesis_iPhone_Quantum Leap_Jethro Tull_Opium_Tocotronic_Chaplin_Tocotronic_Big Bang Theory_Eurythmics_', 'Venus_iPhone_Jethro Tull_Woodstock_Ultravox_Halo_Venus_Spock_X-Files_Woodstock_Kayleigh_Eurythmics_Opium_Neverland_Jethro', 72530, 97199.236, 14085.238 ], 
  [ 19, 'Venus_Kayleigh_Woodstock_Pussy Riot_Big Bang Theory_Tocotronic_Marillion_Zombie_Jethro Tull_Spock_Eurythmics_Woodstock_iPhone_Rails_Depeche Mode_Halo_X-Files_Al Bundy_Pussy Riot_Yello_Venus_Opium_Fringe_Al ', 'Quantum Leap_Eurythmics_Big Bang Theory_Halo_Opium_Jethro Tull_Fringe_Al Bundy_Tocotronic_Big Bang Theory_Halo_Ultravox_Jethro Tull_Halo_Jethro Tull_Z', 62229, 69783.347, 15444.516 ], 
  [ 20, 'Spock_Rails_Ultravox_Lua_Tocotronic_Eurythmics_iPhone_Al Bundy_Lua_Ultravox_Yello_Opium_Opium_Opium_Zombie_Spock_Chaplin_Neverland_Lua_Marillion_', 'Pussy Riot_Depeche Mode_Spock_Neverland_iPhone_Pussy Riot_Al Bundy_X-Files_Tocotronic_Neverland_Kayleigh_Marillion_Eurythmics_Spock_Al Bundy', 69276, 17173.251, 94704.743 ], 
  [ 21, 'Halo_iPhone_Rails_Venus_Lua_Lua_Lua_Big Bang Theory_Jethro Tull_Quantum Leap_Neverland_Fringe_Tocotronic_Rails_Neverland_Jethro Tull_Neverland_Depeche Mode_Kayleigh_Venus_Al Bundy_Opium_Depeche Mode_Tocotronic_', 'Opium_Neverland_Spock_Neverland_Spock_Zombie_Spock_Yello_Jethro Tull_Quantum Leap_Eurythmics_Marillion_Halo_Rails_Spock_Woodstock_Marillion_Ultravox_Eurythmics_Lu', 23402, 2348.528, 87180.339 ], 
  [ 22, 'Rails_Eurythmics_Pussy Riot_Jethro Tull_Venus_Lua_Big Bang Theory_Venus_Depeche Mode_Yello_Woodstock_Venus_Al Bundy_Zo', 'Tocotronic_Chaplin_Venus_Ultravox_Al Bundy_Ultravox_Pussy Riot_Al Bundy', 54720, 62683.30, 31199.577 ], 
  [ 23, 'Quantum Leap_Venus_Tocotronic_Tocotronic_Rails_Depeche Mode_Tocotronic_iPhone_Al Bu', 'Opium_Woodstock_Fringe_Quan', 24097, 87822.89, 37697.594 ], 
  [ 24, 'Kayleigh_Quantum Leap_Ultravox_Big Bang Theory_Zombie_Pussy Riot_Zombie_iPhone_Woodstock_Genesis_Marillion_Eurythmics_Woodstock_Spock_Big Bang Theory_Big Bang Theory_Ultravox_Eurythmics_Big Bang Theory_Gene', 'Fringe_Quantum Leap_iPhone_Halo_iPhone_Al Bundy_Zombie_Fringe_Rails_Fringe_Yello_Big Bang Theory_Ultravox_Halo_Quantum Leap_Jethro Tull_Eurythmics_Tocotronic_Genesis_Neverland', 2430, 28218.484, 93485.967 ], 
  [ 25, 'X-Files_iPhone_Woodstock_Al Bundy_Eurythmics_iPhone_Fringe_Neverland_Venus_Fringe_Opium_Eurythmics_Eurythmics_Quantum Leap_X-Files_Marillion_Halo_Depec', 'Eurythmics_Spock_Chaplin_Big Bang Theory_Jethro Tull_Genesis_Halo_Opium_Jethro Tull_Jethro ', 86668, 28928.918, 32075.747 ], 
  [ 26, 'Ultravox_Yello_Neverland_Ultravox_Venus_Halo_Pussy Riot_iPhone_Chaplin_Big Bang Theory_Rails_Fringe_Lua_Neverland_iPhone_Opium_Zombie_Pussy Riot_Al Bundy_Zombie_Neverland', 'Quantum Leap_Eurythmics_Halo_Eurythmics_Lua_Yello_Depeche Mode_Kayleigh_Woodstock_Fringe_Tocotronic_Ultravox_Woodstock_Venus_iPhone_Opium_Pussy Riot_Chaplin_Ultravox', 10915, 41726.744, 32360.660 ], 
  [ 27, 'Neverland_X-Files_Eurythmics_Lua_Genesis_Kayleigh_Lua_Yello_Yello_Marillion_Eurythmics_Rails_Neverland_X-Files_Pussy Riot_Kayleig', 'Zombie_iPhone_Jethro Tull_Zombie_Venus_Chaplin_Depeche Mode_Chaplin_Ultravox_Ultravox_Ultravox_Jethro Tull_Jethro Tull_Depeche Mode_Genesis_Chaplin_Al Bundy_Depeche Mode_Spock_Spock_Chaplin_P', 93108, 58765.590, 99534.576 ], 
  [ 28, 'Halo_Jethro Tull_', 'Genesis_Venus_Zombie_Halo_Depeche Mode_Big Bang Theory_Quantum Lea', 57879, 65566.535, 35110.919 ], 
  [ 29, 'Chaplin_Eurythmics_Woodstock_Marillion_Yello_Chaplin_Pussy Riot_Depeche Mode_Lua_Depeche Mode_Kayleigh_Depeche Mode_Rails_Kayleigh_Rails_Genesis_Marillion_Opium_Ultravox_Eurythmics_Genesis_Eurythmics_Quantum Leap_Pussy Riot_Ma', 'Al Bundy_iPhone_Ultravox_', 91703, 41316.931, 26592.246 ], 
  [ 30, 'Ultravox_Depeche Mode_Pussy Riot_Pussy Riot_Venus_Rails_Neverland_Venus_Rails_Opium_Jethro Tull_Tocotroni', 'Depeche Mode_Opium_Opium_Spock_Pussy Riot_Genesis_', 77011, 11748.383, 5118.124 ] 
] 

memU_PRIMARY = [] 
@mem_rooms = Array.new
CntSeeds = 0 
Map.delete_all 
rooms_list.each do |val| 
  a = (rand * (@mem_places.count-1)).ceil 
  if memU_PRIMARY.include? (val[0].to_s) 
    puts '  ! Seed reduced by unique attribute, index name: PRIMARY' 
    break 
  end 
  @mem_rooms << Map.create!(id: val[0], name: val[1], url: val[2], floor: val[3], width: val[4], height: val[5]) 
  CntSeeds +=1 
  memU_PRIMARY << (val[0].to_s) 
end 

puts '  * ' + CntSeeds.to_s + ' seed units created for table: rooms' 
puts '--> 30 seed units for table: actions'

actions_list = [ 
  [ 1, 4726.165, 79398.346, 28012.842, 22319, 'Neverland_Genesis_Halo_Halo_Chapli' ], 
  [ 2, 51069.147, 98018.822, 90645.687, 30852, 'Rails_Eurythmics_Fringe_Spock_Chaplin_Chaplin_Al Bundy_Dep' ], 
  [ 3, 18577.234, 84720.857, 29080.369, 47869, 'Rails_Rails_Big Bang Theory_Tocotronic_Fringe_Al Bun' ], 
  [ 4, 88287.236, 71460.185, 6307.876, 64010, 'Opium_Yello_Ye' ], 
  [ 5, 88549.360, 32193.540, 17558.965, 16272, 'Zombie_Eurythmics_Quantum Leap_Rails_Yello_Big Bang Theory_Ultravox_Eurythmics_Lua_Marillion_Depeche Mode_Marillion_iPhone_Jethro Tull_Jethro Tull_Quantum Leap_Spock_Yello_Ultravox_Al B' ], 
  [ 6, 86604.506, 42045.393, 65524.538, 62393, 'Marillion_Yello_Ultravox_Tocotronic_Genesis_Ultravox_Chaplin_Rails_Opium_X-Files_Halo_Big Bang The' ], 
  [ 7, 14164.474, 71965.10, 75223.623, 6454, 'Rails_Marillion_Zombie_Tocotronic_Pussy Riot_Ultravox_Al Bundy_Halo_Marillion_Zombie_Kayleigh_Eurythmics_Fringe_Jethro Tull_Venus_Pussy Riot_Eurythmics_Pussy' ], 
  [ 8, 81747.54, 46718.771, 39445.434, 32584, 'X-Files_Halo_Neverland_Quantum Leap_Depeche Mode_Genesis_Yello_Zombie_Neverl' ], 
  [ 9, 90109.490, 234.276, 99988.903, 2500, 'Big Bang Theo' ], 
  [ 10, 90226.155, 53502.939, 30614.292, 36652, 'Yello_Lua_Rails_Lua_Puss' ], 
  [ 11, 94845.517, 50458.372, 68773.538, 41965, 'Ultravox_Cha' ], 
  [ 12, 95893.580, 68500.642, 10870.816, 6043, 'Neverland_Eurythmics_Jethro Tull_Tocotronic_Quantum Leap_Rails_Chaplin_Tocotronic_Pussy Riot_Yello_Fringe_Big Bang Theory_Pussy Riot_Jethro Tull_Zombie_Eurythmics_' ], 
  [ 13, 81397.265, 90395.564, 4829.481, 99178, 'Depeche Mode_Venus_Marillion_Al Bundy_Fringe_Big Bang Theory_Kayleigh_Al Bundy_Chaplin_Zombie_Lua_Pussy Riot_X-Files_iPhone_Al Bundy_Ultravox_Pussy Riot_Fringe_Kayleigh_Genesis_Mar' ], 
  [ 14, 20232.380, 64581.986, 58680.241, 42915, 'Fringe_Halo_Yello_Al Bundy_Jethro Tull_Marillion_Depeche Mode_Rails_Fringe_Halo_Depeche Mode_Opium_Rails_Spock_Spock_Ultravox_Genesis_Kayleigh_Zombie_Eurythmics_Fringe_Depeche M' ], 
  [ 15, 15612.834, 6125.277, 61913.585, 35188, 'Jethro Tull_Marillion_Depeche Mode_Halo_Neverland_Pussy Riot_Woodstock_Halo_Yello_Al Bundy_Woodstock_Fringe_X-Files_Genesis_Yello_Genesis_X-Files_Depeche Mode_Jethro Tull_Neverland_Al Bundy_Kayleigh_iPhone_Halo_Pussy Riot_Venus_Yello_Yello_Woods' ], 
  [ 16, 10960.916, 51579.854, 13341.137, 63707, 'Spock_Quantum Leap_Woodstock_Woodstoc' ], 
  [ 17, 26118.611, 2437.528, 95885.367, 93322, 'Zombie_Halo_Spock_Pussy Riot_Lua_Eurythmics_Eurythmics_Ultravox_Opium_Opium_iPhone_Al Bundy_Marillion_Tocotronic_Zombie_Marillion_Venus_Ultra' ], 
  [ 18, 13733.30, 36598.858, 36264.730, 64737, 'iPhone_Genesis_Depeche Mode_Venus_Jethro Tull_Chaplin_X-Files_Fringe_Neverland_Genesis' ], 
  [ 19, 51534.204, 18321.67, 50146.981, 50245, 'Jethro Tull_Jethro Tull_iPhone_Halo_Pussy Riot_Lua_Zombie_Marillion_Woodstock_Yello_Venus_Lua_Big Bang Theory_Halo_Spock_Depeche Mode_Ye' ], 
  [ 20, 86635.611, 93651.816, 91861.949, 3501, 'Eurythmics_Halo_Halo_Halo_Opium_Genesis_Al Bundy_Depeche Mode_Al Bundy_Chaplin_Pussy Riot_Fringe_D' ], 
  [ 21, 15837.611, 54932.382, 59490.453, 69000, 'Halo_Depeche Mode_X-Files_Chaplin_Quantum Leap_Zombie_Pussy Riot_Depeche Mode_Quantum Leap_Depeche Mode_Yello_X-Files_Al Bundy_Pussy Riot_Rails_Big Bang Theory_Pussy Riot_Genesis_Ultravox' ], 
  [ 22, 16727.299, 6832.161, 48479.813, 68326, 'Depeche Mode_Neverland_X-Files_Lua_Fringe_Marillion_Genesis_Venus_Fringe_Halo_Quantum Leap_Fringe_Fringe_iPhone_Chaplin_Jethro ' ], 
  [ 23, 791.875, 86180.199, 69123.495, 25767, 'Yello_Chaplin_Halo_Opium_Fringe_Chaplin_X-Files_Woodstock_Pussy Riot_Al Bundy_Venus_Fringe_Al Bundy_Woodstock_X-Files_Quantum Leap_Qu' ], 
  [ 24, 386.866, 23575.95, 24870.861, 44931, 'Pussy Riot_Marillion_Tocotronic_Al Bundy_Genesis_Eurythmics_Woodstock_Opium_Venus_Zombie_Neverland_Rails_Quantum Leap_Rails_Al Bundy_' ], 
  [ 25, 27115.182, 42090.993, 23432.138, 42151, 'Chaplin_iPhone_Lua_Rails_Lua_iPhone_Quantum Leap_K' ], 
  [ 26, 75787.474, 42261.643, 56620.70, 43283, 'Pussy Riot_Quantum Leap_Marillion_Zombie_Genesis_Depeche Mode_Neverland_Lua_Zombie_Zombie_Lua_Venus_Rails_Neverland_Frin' ], 
  [ 27, 30344.782, 93838.195, 3030.146, 30218, 'iPhone_Marillion_Zombie_Tocotronic_Lua_Al Bundy_Rails_Rails_Eurythmics_Chaplin_Genesis_Ultravox_Neverland_Lua_Neverland_Eurythmics_Quantum Leap_Pussy Riot_Depeche Mode_Venus_X-Files_Pussy ' ], 
  [ 28, 70740.200, 6358.488, 37995.700, 84408, 'Genesis_X-Files_Genesis_Eurythmics_Tocotronic_Yello_Venus_Opium_Opium_Marillion_Big Bang Theory_Neverland' ], 
  [ 29, 78245.593, 91553.159, 3735.718, 51071, 'Big Bang Theory_Al Bundy_Big Bang Theory_Big Bang Theory_Chaplin_Zombie_Jethro Tull_Genesis_Al Bundy_Jeth' ], 
  [ 30, 41869.820, 94047.315, 54391.334, 89657, 'Neverland_Jethro Tull_Fringe_Depeche Mode_X-Files_Big Bang Theory_Fringe_Pussy Riot_Fringe_Opium_Eurythmics_Kayleigh_Eurythmics_Quantum Leap_Ultravox_Spock_Big Bang T' ] 
] 

memU_PRIMARY = [] 
CntSeeds = 0 
Action.delete_all 
actions_list.each do |val| 
  a = (rand * (@mem_beacons.count-1)).ceil 
  b = (rand * (@mem_rooms.count-1)).ceil 
  c = (rand * (@mem_resources.count-1)).ceil 
  if memU_PRIMARY.include? (val[0].to_s) 
    puts '  ! Seed reduced by unique attribute, index name: PRIMARY' 
    break 
  end 
  Action.create!(id: val[0], x: val[1], y: val[2], proximity: val[3], type: val[4], content: val[5]) 
  CntSeeds +=1 
  memU_PRIMARY << (val[0].to_s) 
end 

puts '  * ' + CntSeeds.to_s + ' seed units created for table: actions' 
puts ''
puts '*** Seeeds done!'
puts '*** Enjoy your seeded rails project :-)'
puts '*** ... and visit www.marillion.com for a better way of life! Order a FREE CD @ https://marillion.com/account/registerCC.php '
puts ''
