# encoding: utf-8 
class CreateBeacons < ActiveRecord::Migration
  def change
    create_table :beacons do |t| 
      t.string :uuid, :null => false  
      t.integer :major 
      t.integer :minor 
      t.integer :color 
      t.timestamps 
    end
  end
end

