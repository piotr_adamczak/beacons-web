class ChangeTitleToName < ActiveRecord::Migration
  def change
  	rename_column :beacon_placements, :title, :name
  end
end
