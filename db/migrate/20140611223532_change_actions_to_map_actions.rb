class ChangeActionsToMapActions < ActiveRecord::Migration
	def self.up
        rename_table :actions, :map_actions
      end
     def self.down
        rename_table :map_actions, :actions
     end
end
