class CreateBeaconPlacements < ActiveRecord::Migration
  def change
    create_table :beacon_placements do |t|
      t.string :title
      t.integer :beacon_id
      t.integer :map_id
      t.float :x
      t.float :y

      t.timestamps
    end
  end
end
