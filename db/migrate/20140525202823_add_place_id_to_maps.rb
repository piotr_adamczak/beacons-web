class AddPlaceIdToMaps < ActiveRecord::Migration
  def change
    add_column :maps, :place_id, :integer
  end
end
