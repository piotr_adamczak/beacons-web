class ChangeTypeToActionType < ActiveRecord::Migration
  def change
 	rename_column :map_actions, :type, :action_type
   end
end
