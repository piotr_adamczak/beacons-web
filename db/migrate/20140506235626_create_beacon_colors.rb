class CreateBeaconColors < ActiveRecord::Migration
  def change
    create_table :beacon_colors do |t|
      t.string :name

      t.timestamps
    end
  end
end
