# encoding: utf-8 
class CreateMaps < ActiveRecord::Migration
  def change
    create_table :maps do |t| 
      t.text :name 
      t.text :url 
      t.integer :floor 
      t.float :width 
      t.float :height 
      t.integer :place_id
      t.timestamps 
    end
  end
end

