class ChangeNameForImageWidth < ActiveRecord::Migration
  def change
  	 	rename_column :maps, :imageWidth, :image_width 	
  	 	rename_column :maps, :imageheight, :image_height

  end
end
