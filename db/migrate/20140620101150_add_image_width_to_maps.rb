class AddImageWidthToMaps < ActiveRecord::Migration
  def change
  	  	add_column :maps, :imageWidth, :integer
	  	add_column :maps, :imageHeight, :integer
  end
end
