class DeleteVideoUrl < ActiveRecord::Migration
  def change
  	rename_column :map_actions, :video_url, :action_url
  	rename_column :map_actions, :picture_url, :media_url
  end
end
