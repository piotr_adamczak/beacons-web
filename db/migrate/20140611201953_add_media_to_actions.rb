class AddMediaToActions < ActiveRecord::Migration
  def change
  	  	add_column :map_actions, :picture_url, :string
	  	add_column :map_actions, :video_url, :string
  end
end
