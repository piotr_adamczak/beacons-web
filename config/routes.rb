Beacons::Application.routes.draw do
  devise_for :admins
  get '/' => "maps#index"
  resources :beacons
  resources :places
  resources :maps
  resources :uploads
  resources :map_actions
  resources :beacon_placements
  get "api/beacons"
  get "api/setup"
  get 'api/objects_for_map/:id' => 'api#objects_for_map'
  get 'api/beacons_on_map/:id' => 'api#beacons_on_map'
end
