# encoding: utf-8 
class Resource < ActiveRecord::Base 

  attr_accessible  :id, :url, :type

  has_many :map_actions, dependent: :destroy 


end