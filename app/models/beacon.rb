# encoding: utf-8 
class Beacon < ActiveRecord::Base 

  attr_accessible  :id, :uuid, :major, :minor, :color, :displayed_name

  validates :uuid, :presence => true
 	 
  def displayed_name
     "#{self.uuid}-#{self.major}-#{self.minor}"
  end
end