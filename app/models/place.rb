# encoding: utf-8 
class Place < ActiveRecord::Base 

  attr_accessible  :id, :name, :maps_count

  has_many :maps, dependent: :destroy 
  validates :name, :presence => true

  def maps_count
     Map.where(place_id: self.id).count;
  end
end