# encoding: utf-8 
class Action < ActiveRecord::Base 

  attr_accessible  :id, :x, :y, :proximity, :type, :content

  belongs_to :beacon 
  belongs_to :map 
  belongs_to :resource 


end