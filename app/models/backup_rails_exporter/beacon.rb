# encoding: utf-8 
class Beacon < ActiveRecord::Base 

  attr_accessible  :id, :uuid, :major, :minor, :color

  has_many :map_actions, dependent: :destroy 

  validates :uuid, :presence => true

end