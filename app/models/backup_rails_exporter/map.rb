# encoding: utf-8 
class Map < ActiveRecord::Base 

  attr_accessible  :id, :name, :url, :floor, :width, :height, :place_id

  belongs_to :place 
  has_many :map_actions, dependent: :destroy 


end