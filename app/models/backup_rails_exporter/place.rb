# encoding: utf-8 
class Place < ActiveRecord::Base 

  attr_accessible  :id, :name

  has_many :rooms, dependent: :destroy 


end