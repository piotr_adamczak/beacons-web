# encoding: utf-8 
class Map < ActiveRecord::Base 

  attr_accessible  :id, :name, :url, :floor, :width, :height, :place_id, :place_name, :image_height, :image_width, :map_type

  belongs_to :place 
  has_many :map_actions, dependent: :destroy 
  validates :name, :presence => true
  validates :width, :presence => true
  validates :height, :presence => true

  def place_name
     Place.find(self.place_id).name;
  end

end