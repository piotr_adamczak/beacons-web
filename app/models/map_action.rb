# encoding: utf-8 
class MapAction < ActiveRecord::Base 
  
  attr_accessible  :x, :y, :name, :proximity, :action_type, :content, :map_id, :media_url, :action_url, :beacon_id
  validates :name, :presence => true
end