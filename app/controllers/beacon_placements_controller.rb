class BeaconPlacementsController < InheritedResources::Base
  respond_to :html, :xml, :json

  def permitted_params
    params.permit(:beacon_placement => [:name, :x, :y, :beacon_id, :map_id, :action_id])
  end

  def new 
    @beacon_placement = BeaconPlacement.new
    render :layout => false
  end

  def edit 
    @beacon_placement = BeaconPlacement.find(params[:id])
    render :layout => false
  end
  
  def show 
    @beacon_placement = BeaconPlacement.find(params[:id])
    render :layout => false
  end
end
