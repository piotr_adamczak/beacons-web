class MapActionsController < InheritedResources::Base
  respond_to :html, :xml, :json

  def permitted_params
    params.permit(:map_action => [:x, :y, :name, :proximity, :action_type, :content, :map_id, :media_url, :action_url,:beacon_id])
  end

  def new 
    @map_action = MapAction.new
    render :layout => false
  end

  def edit 
    @map_action = MapAction.find(params[:id])
    render :layout => false
  end
  
  def show 
    @map_action = MapAction.find(params[:id])
    render :layout => false
  end
end
