class ApiController < ApplicationController

	def beacons 
        render json: {"beacons" => Beacon.all}
	end

	def setup 
        render json: {"places" => Place.all,
        			  "maps" => Map.all}
	end

	def objects_for_map
		render json: {"beacons" => BeaconPlacement.find(:all, :conditions => { :map_id => params[:id]}),
					  "actions" => MapAction.find(:all, :conditions => { :map_id => params[:id]})}
	end

	def beacons_on_map 
		render json: {"beacons" => Beacon.find(BeaconPlacement.find(:all, :conditions => { :map_id => params[:id]}, :select => :beacon_id).map(&:beacon_id))}
	end
end
