class PlacesController  < InheritedResources::Base
  respond_to :html, :xml, :json
  before_action :authenticate_admin!

  def permitted_params
    params.permit(:place => [:name])
  end

  def index
  	@places = Place.all
  end

  def new 
  	@place = Place.new
  end

  def edit 
    @place = Place.find(params[:id])
  end
end
