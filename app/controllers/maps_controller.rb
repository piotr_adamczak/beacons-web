class MapsController < InheritedResources::Base
  respond_to :html, :xml, :json
  before_action :authenticate_admin!
 
  def permitted_params
    params.permit(:map => [:name, :url, :floor, :width, :height, :place_id, :image_width, :image_height, :map_type])
  end

  def index
  	@maps = Map.all
  end

  def new 
  	@map = Map.new
  end

  def edit 
    @map = Map.find(params[:id])
  end
end
