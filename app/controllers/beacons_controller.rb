class BeaconsController  < InheritedResources::Base
  respond_to :html, :xml, :json
  skip_before_filter  :verify_authenticity_token
  
  def permitted_params
    params.permit(:beacon => [:uuid, :major, :minor, :color])
  end


  def index
  	@posts = Beacon.all
  end

  def new 
  	@beacon = Beacon.new
  end

  def edit 
    @beacon = Beacon.find(params[:id])
  end

end
