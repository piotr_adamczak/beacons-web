
function reloadPlayer(type,url) { 

	$(".mejs-container").hide();
	$("#player").hide();
	$("#audioPlayer").hide();
	$("#mediaImage").hide();

	mediaType = (type == 2) ? '#player' : '#audioPlayer';

    if (type == 0) {
        return null;
    } else if (type == 1) {
		$("#mediaImg").attr("src", url);
	    $("#mediaImage").show();
	    return null;
	} else if (type == 2) {
		$("#player").attr("src",url);
		$("#player").show();
	} else if (type == 3){
		$("#audioPlayer").attr("src",url);	
		$("#audioPlayer").show();
	}

    return new MediaElementPlayer(mediaType,{
    // if the <video width> is not specified, this is the default
    defaultVideoWidth: 350,
    // if the <video height> is not specified, this is the default
    defaultVideoHeight: 200,
    // width of audio player
    audioWidth: 350,
    // height of audio player
    audioHeight: 30,
    // initial volume when the player starts
    startVolume: 0.8,
    // useful for <audio> player loops
    loop: false});
}

function typeForName(name) {
	var extension = name.substr( (name.lastIndexOf('.') +1) );
    switch(extension) {
        case 'jpg':
        case 'png':
        case 'gif':
        case 'bmp':
        case 'tiff':
        case 'jpeg':
        	return 1;
        break;                        
        case 'mp4':
            return 2;
        break;
        case 'mp3':
            return 3;
        break;
        default:
            return 0;
    }
}

function setupMapActions() {

   var player;

    setupMapActionBeacon();

   var mediaUploaderOptions = {
            dataType:  'json',
            uploadProgress: OnMediaUploadProgress,
            success: function(json){
                if (json.files[0].name) {
                	url = json.files[0].url;
                	name = json.files[0].name;
                	type = typeForName(name);
					player = reloadPlayer(type,url);
					$("#map_action_action_type").val(type);
					$("#map_action_media_url").val(url);
					$('#media-fileupload').clearForm();
            	}
            }
    };

	$("#mediaImage").hide();

	reloadPlayer($("#map_action_action_type").val(),$("#map_action_media_url").val());

    $('.fileinput-button-beacon').click(function() {
    	$('#errors').html("");
    });

     $('#upload_media').click(function() {
          $("#media-progress-bar").fadeIn();
          $("#media-fileupload").ajaxForm(mediaUploaderOptions).submit();
          return false;
    });

   $('#media-progress-bar').progressbar().hide();

    function OnMediaUploadProgress(event, position, total, percentComplete)
    {
        //Progress bar
        $('#media-progress-bar').progressbar("option", "value", percentComplete);
   
        if (percentComplete == 100) {
            $('#media-progress-bar').fadeOut();
        }
    }

    $('#map_action_content').height(225);
}