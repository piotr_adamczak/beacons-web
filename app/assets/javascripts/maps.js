//= require jquery.form
//= require jquery.ui.dialog
//= require jquery.ui.progressbar
//= require mediaelement-and-player.min
//= require map_actions

function readyFn() {

 	var viewer;
    var beacons = [];
    var tempBeacon = [];
    var beaconLocation = ($("#map_map_type").val() == 0) ? 1 : 0;
	var options = {
	        dataType:  'json',
            uploadProgress: OnMapUploadProgress,
	        success: function(json){
           	    url = json.files[0].url;

                var img = $('<img src="'+url+'"/>').load(function(){
                    $("#map_image_width").val(this.width);
                    $("#map_image_height").val(this.height);
                });

                $("#map_url").val(url);
	        	viewer.iviewer('loadImage',url);
	            $(form).clearForm();
	        }
	};

 
    var dragging = null;
    var currentObject = null;

    $('#upload_pic').click(function() {
          $("#map-progress-bar").fadeIn();
          $("#fileupload").ajaxForm(options).submit();
          return false;
    });

    $('#map-progress-bar').progressbar().hide();
 
    function OnMapUploadProgress(event, position, total, percentComplete)
    {
        //Progress bar
        $('#map-progress-bar').progressbar("option", "value", percentComplete);
 
        if (percentComplete == 100) {
            $('#map-progress-bar').fadeOut();
        }
    }

    function closeBeaconEditModal() {
        refreshObjects();
        $("#beacon_edit_form").dialog("close");
    }
   
    function closeActionEditModal() {
        refreshObjects();
        $("#map_action_edit_form").dialog("close");
    }

    //Modal
    $("#beacon_edit_form").dialog({
      autoOpen: false,
      height: 400,
      width: 300,
      modal: true,
      show: 'drop',
      hide: 'drop',
      buttons: {
        Save: function() {
            
            var beaconOptions = { 
                success: closeBeaconEditModal  // post-submit callback 
            }; 
            $(".edit_beacon_placement").ajaxSubmit(beaconOptions);

        },
        Delete: function() {
            
            $.ajax({
                url: $(".edit_beacon_placement").attr('action'),
                headers : {
                    'Content-Type' : 'application/json'
                },
                type: 'DELETE',
                complete: function(result) {
                   refreshObjects();
                }
            });
           $(this).dialog( "close" );
 
        },
        Cancel: function() {
            $(this).dialog( "close" );
        }
      },
      close: function() {
        currentObject = null;
      }
    });

//Modal
    $("#map_action_edit_form").dialog({
      autoOpen: false,
      height: 450,
      width: 850,
      modal: true,
      show: 'drop',
      hide: 'drop',
      buttons: {
        Save: function() {
            
            var actionOptions = { 
                success: closeActionEditModal  // post-submit callback 
            }; 
            $(".edit_map_action").ajaxSubmit(actionOptions);

        },
        Delete: function() {
            
            $.ajax({
                url: $(".edit_map_action").attr('action'),
                headers : {
                    'Content-Type' : 'application/json'
                },
                type: 'DELETE',
                complete: function(result) {
                   refreshObjects();
                }
            });
           $(this).dialog( "close" );
 
        },
        Cancel: function() {
            $(this).dialog( "close" );
        }
      },
      close: function() {
        currentObject = null;
      }
    });

    function closeBeaconAddModal() {
        refreshObjects();
        $("#beacon_new_form").dialog("close");
    }
  
    function closeActionAddModal() {
        refreshObjects();
        $("#map_action_new_form").dialog("close");
    }
     //Add action Modal
    $("#map_action_new_form").dialog({
      autoOpen: false,
      height: 450,
      width: 850,
      modal: true,
      show: 'drop',
      hide: 'drop',
      buttons: {
        Save: function() {
            
            var actionOptions = { 
                success: closeActionAddModal  // post-submit callback 
            }; 
            $(".new_map_action").ajaxSubmit(closeActionAddModal);

        },
        Cancel: function() {
            $(this).dialog( "close" );
        }
      },
    });

      //Add beacon Modal
    $("#beacon_new_form").dialog({
      autoOpen: false,
      height: 400,
      width: 300,
      modal: true,
      show: 'drop',
      hide: 'drop',
      buttons: {
        Save: function() {
            
            var beaconOptions = { 
                success: closeBeaconAddModal  // post-submit callback 
            }; 
            $(".new_beacon_placement").ajaxSubmit(beaconOptions);

        },
        Cancel: function() {
            $(this).dialog( "close" );
        }
      },
    });

    function setupMapActionBeacon() {  
        var optionsHTML = [];
        for (var i=0; i<beacons.length; i++) {
            var obj = beacons[i];
            optionsHTML.push('<option value="',obj.id, '">',obj.uuid+"-"+obj.major+"-"+obj.minor, '</option>');
        }
        $("#map_action_beacon_id").html(optionsHTML.join('')); 
    }

    function setupMapActionViews() {
        if (beaconLocation) {
            $("#map_action_beacon_id").show();
            $(".beaconData").show();
            $(".locationData").hide();
        } else {
            $("#map_action_beacon_id").hide();
            $(".beaconData").hide();
            $(".locationData").show();
        }
    }

    function handleClickOnObject(object) {
        currentObject = object;

        if (currentObject.type == "beacon") { 
            $("#beacon_edit_form").load(editUrlForObject(object));
            $("#beacon_edit_form").dialog("open");
        } else {
            $("#map_action_edit_form").load(editUrlForObject(object), function() {
                setupMapActions();   
                setupMapActionViews();
            });
            $("#map_action_edit_form").dialog("open");
        }
    }

    $("#add_beacon").click(function() {
        $("#beacon_new_form").load("/beacon_placements/new",function() {
            $("#beacon_placement_map_id").val($("#map_id").val());
        });
        $("#beacon_new_form").dialog("open");
    });


    $("#add_action").click(function() {
        $("#map_action_new_form").load("/map_actions/new",function() {
            $("#map_action_map_id").val($("#map_id").val());
            setupMapActionBeacon();
            setupMapActionViews();
            setupMapActions();
            setupMapActionBeacon();
 
        });
        $("#map_action_new_form").dialog("open");
    });

    function editUrlForObject(object) {
        if (object.type == "beacon") {
            return "/beacon_placements/"+object.id+"/edit";
        } else if(object.type == "action") {
            return "/map_actions/"+object.id+"/edit";
        }
    }

    function patchUrlForObject(object) {
        if (object.type == "beacon") {
            return "/beacon_placements/"+object.id;
        } else if(object.type == "action") {
            return "/map_actions/"+object.id;
        }
    }
 
    function isInCircle(x, y) {   
        var relative_x = x - this.x;
        var relative_y = y - this.y;
        return Math.sqrt(relative_x*relative_x + relative_y*relative_y) <= this.r;
    }
    
    function isInRectangle(x, y) {
        return (this.x1 <= x && x <= this.x2) && (this.y1 <= y && y<= this.y2);
    }

    function isInBeacon(x, y) {

        return (this.x-17 <= x && x <= this.x+17) && (this.y-22 <= y && y<= this.y+22);
    }
    
    function getCircleCenter() { 
        
        var realWidth = $("#map_width").val();
        var realHeight = $("#map_height").val();
        var multiplierX = this.x / realWidth;
        var multiplierY = this.y / realHeight;
        var originWidth = viewer.iviewer('info', 'orig_width');
        var originHeight = viewer.iviewer('info', 'orig_height');   
     
        var centerX;
        var centerY;
        centerX = multiplierX * originWidth;
        centerY = multiplierY * originHeight;

        return {x: centerX, y: centerY}; 
    }
    
    function getRectangleCenter() { return {x: (this.x2+this.x1)/2, y: (this.y2+this.y1)/2}; }

    function getBeaconCenter () { return {x: this.x, y:this.y};}

    var objects = [ ];

    function objectWithId(id) {
         for (var i=0; i<objects.length; i++) {
            var obj = objects[i];
            if (idForObject(obj) == id) {
                return obj;
            }
         }
    }

    function beaconWithId(id) {
        for (var i=0; i<tempBeacon.length; i++) {
            var obj = tempBeacon[i];
            if (obj.type == "beacon") {
                if (obj.beacon_id == id) {
                    console.log(obj);
                    return obj;
                }
            }
         }
         return null;
    }

    function refreshObjects() {
        $('.beaconObject').remove();
        $('.actionObject').remove();

        mapID = $("#map_id").val();
        url = "/api/objects_for_map/"+mapID;
        
        tempBeacon = [];
        objects.splice(0, objects.length);

        $.getJSON(url, function(json) {
            $.each(json.beacons, function( i, beacon ){
              beacon.type = "beacon";
              beacon.isInObject = isInBeacon;
              beacon.getCenter = getCircleCenter;
              tempBeacon.push(beacon);
            });

            $.each(json.actions, function( i, action ){
              action.type = "action";
              action.isInObject = isInCircle;
              action.getCenter = getCircleCenter;
              action.r = action.proximity;

              if (beaconLocation == 0) {
                 action.beacon_id = null;
              } 

              if (action.beacon_id != null) {
                 beacon = beaconWithId(action.beacon_id);
                 if (beacon) {
                    action.x = beacon.x;
                    action.y = beacon.y;
                }
              }

              if (beaconLocation == 1 && action.beacon_id >= 0) {
                console.log(action);
                    objects.push(action);
              } else if (beaconLocation == 0 && (action.beacon_id == null || action.beacon_id < 0)) {
                    objects.push(action);
              }
            });

            $.each(tempBeacon, function(i, beacon) {
                objects.push(beacon);
            });

            addObjects();
            showAllObjects();
        });

        url = "/api/beacons_on_map/"+mapID;
        $.getJSON(url, function(json) {
           $.each(json.beacons, function( i, beacon ){ 
              beacons.push(beacon);
           });
        });
    }

    function addObjects() {
         for (var i=0; i<objects.length; i++) {
            var obj = objects[i];
            if (obj.type == "action") {
                addObjectWithType(idForObject(obj),"actionObject");
            } else if (obj.type == "beacon") {
                addObjectWithType(idForObject(obj),"beaconObject");
            }
        }

        $(".map_marker").dblclick(function(event) {
            handleClickOnObject(objectWithId(event.target.id));
        });

        $(".map_marker").on("mousemove", function (e) {

            if (dragging != null) 
                {
                    var coords = viewer.iviewer('info','coords');
                    var offset = $(".wrapper").offset();
                    var zoom = viewer.iviewer('info','zoom') / 100;
                    var realWidth = $("#map_width").val();
                    var realHeight = $("#map_height").val();
                    var multiplierX = dragging.x / realWidth;
                    var multiplierY = dragging.y / realHeight;
                    var originWidth = viewer.iviewer('info', 'display_width');
                    var originHeight = viewer.iviewer('info', 'display_height');   

                    var relativePosX = e.pageX - coords.x - offset.left;
                    var relativePosY = e.pageY - coords.y - offset.top;

                    var imageMultiplierX = relativePosX / originWidth;
                    var imageMultiplierY = relativePosY / originHeight;

                    object = objectWithId(dragging);
                    object.x = imageMultiplierX * realWidth;
                    object.y = imageMultiplierY * realHeight;

                    $("#log").html(object.x+" "+object.y);
   
                    showObject(object);
                }
        });

        $(".map_marker").on("mousedown", function (e) {

            if (dragging == null) {
                var realObject = objectWithId(e.target.id);
                if (realObject.type != "action" || beaconLocation == 0) { 
                    dragging = e.target.id;
                    $("#"+dragging).addClass("shadow");
                } else {
                    dragging = null;
                }
            }
        });

        $(".map_marker").on("mouseup", function (e) {

            if (dragging != null) {
                object = objectWithId(dragging);
                $.ajax({
                    contentType: 'application/json; charset=utf-8',
                    headers: {          
                     Accept : "application/json; charset=utf-8",         
                            "Content-Type": "application/json; charset=utf-8"   
                    },     
                    url : patchUrlForObject(object),
                    beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},                
                    type : 'PATCH',
                    data : JSON.stringify({x: object.x, y: object.y}),
                    success : function(response, textStatus, jqXhr) {
                        console.log(response);
                    }
                });

      
                $("#"+dragging).removeClass("shadow");
                dragging = null;
                //refreshObjects();
            }
        });
    }

    function idForObject(object) {
        return object.type+object.id;
    }
    
    function addObjectWithType(id,type) {
        var $newDiv = $("<div/>").attr("id", id).addClass("map_marker").addClass(type);
        $(".wrapper").append($newDiv);
    }

    function whereIam(x, y) {
        for (var i=0; i<objects.length; i++) {
            var obj = objects[i];
            if (obj.isInObject(x, y))
                return obj;
        }
        return null;
    }
    
    function showMe(ev, a) {
        $.each(objects, function(i, object) {
            if (object.name == $(a).html()) {
               showObject(object);
            }                    
        });

        ev.preventDefault();
    }

    function showAllObjects() {
    	for (var i=0; i<objects.length; i++) {
            var obj = objects[i];
            showObject(obj);
        }
    }

    function showObject(obj) {
        if (obj.type == "beacon") {
            showBeacon(obj);
        } else if (obj.type == "action") {
            showAction(obj);
        }
    }

    function showBeacon(object) {
        var center = object.getCenter();
        var offset = viewer.iviewer('imageToContainer', center.x, center.y);
        var pointer = $("#"+idForObject(object));
 
        offset.x -= 17;
        offset.y -= 22;
        pointer.css('display', 'block');
        pointer.css('left', offset.x+'px');
        pointer.css('top', offset.y+'px');
    }

    function showAction(object) {
        var center = object.getCenter();

        console.log(object);

        var offset = viewer.iviewer('imageToContainer', center.x, center.y);
        console.log(offset);

        var pointer = $("#"+idForObject(object));
        var zoom = viewer.iviewer('info','zoom') / 100;
        var originWidth = viewer.iviewer('info', 'display_width');
        var realWidth = $("#map_width").val();
        var multiplier = object.r / realWidth;
        var realRadius = multiplier * originWidth;
    
        console.log(offset.x+" "+offset.y+" "+realRadius);
        offset.x -= realRadius;
        offset.y -= realRadius;
        pointer.css('display', 'block');
        pointer.css('width', realRadius*2);
        pointer.css('height', realRadius*2);
        pointer.css('left', offset.x+'px');
        pointer.css('top', offset.y+'px');
    }

    function checkDragging(coords) {
        if (dragging != null) {
               
            var realWidth = $("#map_width").val();
            var realHeight = $("#map_height").val();
            var multiplierX = dragging.x / realWidth;
            var multiplierY = dragging.y / realHeight;
            var originWidth = viewer.iviewer('info', 'display_width');
            var originHeight = viewer.iviewer('info', 'display_height');   
            var zoom = viewer.iviewer('info','zoom') / 100;
 
            var relativePosX = coords.x;
            var relativePosY = coords.y;

            var imageMultiplierX = relativePosX / originWidth;
            var imageMultiplierY = relativePosY / originHeight;

            object = objectWithId(dragging);
            object.x = imageMultiplierX * realWidth * zoom;
            object.y = imageMultiplierY * realHeight * zoom;
            showObject(object);
        }
    }

    window.showMe = showMe;

    viewer = $("#viewer").iviewer({
        src: $("#map_url").val(),

        onClick: function(ev, coords) {
            var object = whereIam(coords.x, coords.y);
       },

        onMouseMove: function(ev, coords) {
            showAllObjects();
            checkDragging(coords);                           
        },
        
        onBeforeDrag: function(ev, coords) {
            // remove pointer if image is getting moving
            // because it's not actual anymore
            $('.map_marker').css('display', 'none');
            // forbid dragging when cursor is whithin the object
            return whereIam(coords.x, coords.y) ? false : true;     
        },
        
        onStopDrag: function(ev, coords) {
        	showAllObjects();
        },

        onAfterZoom:function(ev, coords) {
            showAllObjects();
        },

        onFinishLoad:function(ev) {
            refreshObjects();
        },

        onZoom: function(ev) {
            // remove pointer if image is resizing
            // because it's not actual anymore
            $('.map_marker').css('display', 'none');
            showAllObjects();
        },
        
        initCallback: function(ev) {
            this.container.context.iviewer = this;
        }
    });
}

$(document).ready(readyFn);